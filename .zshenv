# Source prezto
if [[ "$SHLVL" -eq 1 && ! -o LOGIN && -s "${ZDOTDIR:-$HOME}/.zprofile" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprofile"
fi

export EDITOR='vim'
export VISUAL='vim'
export PAGER='less'

# Setup anyenv
if [ -d ${HOME}/.anyenv ] ; then
  export PATH="$HOME/.anyenv/bin:$PATH"
  eval "$(anyenv init -)"
  for D in `ls $HOME/.anyenv/envs`
  do
    export PATH="$HOME/.anyenv/envs/$D/shims:$PATH"
  done
fi

# Setup npm
if [ -d ${HOME}/.npm ] ; then
    export PATH=$PATH:`npm bin -g`
fi

# Setup pandoc
if [ -d ${HOME}/.cabel ] ; then
    export PATH=${HOME}/.cabal/bin:$PATH
fi

typeset -U path PATH

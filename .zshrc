# Source prezto
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
    source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Setup keybind
bindkey '^P' history-beginning-search-backward
bindkey '^N' history-beginning-search-forward

# Setup prompt
RPROMPT="%F{magenta}[%*]%f"

# Setup tmux
if [ -z "$TMUX" -a -z "$STY" ]; then
    alias tmux="TERM=xterm-256color tmux"
    if type tmuxx >/dev/null 2>&1; then
        tmuxx
    elif type tmux >/dev/null 2>&1; then
        if tmux has-session && tmux list-sessions | /usr/bin/grep -qE '.*]$'; then
            tmux attach && echo "tmux attached session "
        else
            tmux new-session && echo "tmux created new session"
        fi
    elif type screen >/dev/null 2>&1; then
        screen -rx || screen -D -RR
    fi
fi

# Setup direnv
eval "$(direnv hook zsh)"

# Setup fuck
eval "$(thefuck --alias)"

# Setup peco
function peco-select-history() {
    local tac
    if which tac > /dev/null; then
        tac="tac"
    else
        tac="tail -r"
    fi
    BUFFER=$(\history -n 1 | \
        eval $tac | \
        peco --query "$LBUFFER")
    CURSOR=$#BUFFER
    zle clear-screen
}
zle -N peco-select-history
bindkey '^r' peco-select-history


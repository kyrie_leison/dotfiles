" --------------------------------
" NeoBundle 設定
" --------------------------------

if 0 | endif

if has('vim_starting')
    if &compatible
        set nocompatible
    endif

    " Required:
    set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" My Bundles here:

" 入力補完機能
if has('lua') && (( v:version == 703 && has('patch885')) || (v:version >= 704))
  NeoBundle 'Shougo/neocomplete'
else
  NeoBundle 'Shougo/neocomplcache'
endif
" スニペット機能
NeoBundle "Shougo/neosnippet"
NeoBundle "Shougo/neosnippet-snippets"
" 非同期処理の高速化
NeoBundle 'Shougo/vimproc', {
  \ 'build' : {
  \     'windows' : 'make -f make_mingw32.mak',
  \     'cygwin' : 'make -f make_cygwin.mak',
  \     'mac' : 'make -f make_mac.mak',
  \     'unix' : 'make -f make_unix.mak',
  \    },
  \ }
" ファイルビューア
NeoBundleLazy 'Shougo/vimfiler', {
  \ 'depends' : ["Shougo/unite.vim"],
  \ 'autoload' : {
  \   'commands' : [ "VimFilerTab", "VimFiler", "VimFilerExplorer", "VimFilerBufferDir" ],
  \   'mappings' : ['<Plug>(vimfiler_switch)'],
  \   'explorer' : 1,
  \ }}
" カーソル移動拡張
NeoBundle 'Lokaltog/vim-easymotion'
" ペイストモード拡張
NeoBundle 'ConradIrwin/vim-bracketed-paste'
" 囲み文字拡張
" NeoBundle 'tpope/vim-surround'
" 閉じ括弧の自動挿入
NeoBundle 'Townk/vim-autoclose'
" インデント可視化
NeoBundle 'Yggdroot/indentLine'
" マークダウンプレビュー
NeoBundle 'plasticboy/vim-markdown'
NeoBundle 'kannokanno/previm'
NeoBundle 'tyru/open-browser.vim'
" テキステイルプレビュー
NeoBundle 'timcharper/textile.vim'
" シンタックス(HTML/CSS)
NeoBundle 'othree/html5.vim'
NeoBundle 'hail2u/vim-css3-syntax'
NeoBundle 'AtsushiM/search-parent.vim'
NeoBundle 'AtsushiM/sass-compile.vim'
NeoBundle 'wavded/vim-stylus'
" シンタックス(JavaScript)
NeoBundle 'pangloss/vim-javascript'
NeoBundle 'kchmck/vim-coffee-script'
NeoBundle 'moll/vim-node'
" シンタックスチェック
NeoBundle 'scrooloose/syntastic'
" カラースキーマ
NeoBundle 'w0ng/vim-hybrid'
" ステータスライン拡張
NeoBundle 'itchyny/lightline.vim'
NeoBundle 'cocopon/lightline-hybrid.vim'

" vim-markdown {{{
let g:vim_markdown_folding_disabled=1
" }}}

" indentLine {{{
let g:indentLine_color_term = 239
" }}}

" lightline {{{
let g:lightline = {}
let g:lightline.colorscheme = 'hybrid'
" }}}

call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck

" --------------------------------
" 基本設定
" --------------------------------

syntax on

set number
set ruler
set list
set incsearch
set hlsearch
set wrap
set showmatch
set showmode
set wildmenu
set whichwrap=h,l
set nowrapscan
set ignorecase
set smartcase
set hidden
set history=50
set helplang=en
set mouse=a
set backspace=indent,eol,start
set clipboard=unnamed,autoselect
set encoding=utf-8
set fileencodings=utf-8,sjis,euc-jp,iso-2022-jp,latin1
set fileformats=unix,dos,mac
set formatoptions+=mM
set ambiwidth=double
set display+=lastline
set clipboard=unnamed,autoselect
set undofile
set undodir=./.vimundo,~/.vimundo

" カーソルライン設定
set cursorline
augroup cch
    autocmd! cch
    autocmd WinLeave * set nocursorline
    autocmd WinEnter,BufRead * set cursorline
augroup END

hi clear CursorLine
hi CursorLine ctermbg=black guibg=black

" ステータスライン設定
set laststatus=2
" set statusline=%<%f\ %m\ %r%h%w%{'['.(&fenc!=''?&fenc:&enc).']['.&ff.']'}%=\ (%v,%l)/%L%8P\ 

" カラースキーマ設定
set background=dark
colorscheme hybrid

" シンタックスハイライト設定
au BufRead,BufNewFile *.md\|txt    set filetype=markdown
au BufRead,BufNewFile *.textile    set filetype=textile
au BufRead,BufNewFile *.tx         set filetype=html
au BufRead,BufNewFile *.erb        set filetype=html
au BufRead,BufNewFile *.sass\|scss set filetype=sass

" インデント設定
set autoindent
set smartindent
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set softtabstop=4

au FileType perl       setlocal sw=4 ts=2 sts=2
au FileType html       setlocal sw=2 ts=2 sts=2
au FileType css        setlocal sw=2 ts=2 sts=2
au FileType javascript setlocal sw=4 ts=2 sts=2
au FileType markdown   setlocal sw=4 ts=4 sts=4
au FileType ruby       setlocal sw=2 ts=2 sts=2
au FileType haml       setlocal sw=2 ts=2 sts=2
au FileType less,sass  setlocal sw=2 sts=2 ts=2

" キーマップ設定
noremap j gj
noremap k gk

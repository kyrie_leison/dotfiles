#!/bin/sh

set -eu

# Deploy files
for f in `ls . `
do
    [[ "$f" == "README.md" ]] && continue
    [[ "$f" == "build.sh" ]] && continue
    [[ "$f" == "deploy.sh" ]] && continue
    [[ "$f" == ".DS_Store" ]] && continue
    [[ "$f" == ".git" ]] && continue

    ln -s -v -f $f $HOME
    echo "Create symbolic link: $f"
done

# Setup vim
mkdir -p ~/.vim/backup ~/.vim/tmp
if [ ! -d ~/.vim/bundle/neobundle.vim ]; then
    echo "Install neobundle"
    git clone git://github.com/Shougo/neobundle.vim.git $HOME/.vim/bundle
fi
vim -u ~/.vimrc -i NONE -c "try | NeoBundleUpdate! | finally | q! | endtry" -e -s -V1

